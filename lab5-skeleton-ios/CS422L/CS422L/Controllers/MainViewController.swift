//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    
    var sets: [FlashcardSet] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSet")
        do{
            let myFCS = try managedContext.fetch(fetchRequest) as! [FlashcardSet]
            updateData(set: myFCS)
        } catch{
            print(error)
        }
        
        
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
    }

    
    @IBAction func addNewSet(_ sender: Any) {
        
        
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let fcS = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSet", into: managedContext) as! FlashcardSet
        fcS.title = "title \(sets.count + 1)"
        
        do{
            managedContext.insert(fcS)
            try managedContext.save()
            sets.append(fcS)
            collectionView.reloadData()
            
            
        } catch{
            print(error)
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    
    func updateData(set: [FlashcardSet]) {
        self.sets = set
        self.collectionView.reloadData()
    }
    
}

