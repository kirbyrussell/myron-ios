//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate
{
    var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    @IBOutlet var alertView: UIView!
    
    @IBOutlet var termTextField:UITextField!
    @IBOutlet var defTextField: UITextField!
    
    var selectedIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        
        termTextField?.text = cards[selectedIndex].term
        
        defTextField?.text = cards[selectedIndex].definition
        
        let longClick = UILongPressGestureRecognizer(target: self, action: #selector(LongClick))
        longClick.minimumPressDuration = 0.5
        self.tableView.addGestureRecognizer(longClick)
        
       
        makeItPretty()
    }
    
    @objc func LongClick(longClick:UILongPressGestureRecognizer, indexPath:IndexPath){
        let l = longClick.location(in: parent?.view)
        
        let ip = self.tableView.indexPathForRow(at: l)
        createAlert_1(indexPath: indexPath)
        if longClick.state == UIGestureRecognizer.State.began {
            selectedIndex = ip?.row ?? 0
        }
            
        }
    
    func createAlert_1(indexPath: IndexPath){
        let alert = UIAlertController(title: "\(cards[indexPath.row].term)", message: "\(cards[indexPath.row].definition)", preferredStyle: .alert)
        selectedIndex = indexPath.row
        
        
        
        
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in alert.dismiss(animated:true, completion: {}); self.edit(indexPath: indexPath) }))
            self.present(alert, animated: true)
        
        
        
    }
    
    
        
    
   
    
    
    
    func createAlert_2(indexPath: IndexPath){
        let alert = UIAlertController(title: "\(cards[indexPath.row].term)", message: "\(cards[indexPath.row].definition)", preferredStyle: .alert)
        
        selectedIndex = indexPath.row
        
        
        
        alert.addTextField{(termTextField)in termTextField.placeholder = "Enter term"}
            
        
        alert.addTextField{(defTextField)in defTextField.placeholder = "Enter definition"};
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: {_ in alert.dismiss(animated: true, completion: {})}));
            
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {_ in
            let termField = alert.textFields?[0]
            let defField = alert.textFields?[1]
            self.cards[self.selectedIndex].term = termField?.text ?? ""
            self.cards[self.selectedIndex].definition = defField?.text ?? ""
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
            cell.flashcardLabel.text = termField?.text
            self.tableView.reloadData()
        }));
        self.present(alert, animated: true, completion: {})
        
        
            
                /*
         alert.addAction(UIAlertAction(title: //"Cancel", style: .cancel, handler: nil))
         */
    
    
        
    
    }
    
    
    
        
    func edit(indexPath:IndexPath){
        
        createAlert_2(indexPath: indexPath)
        
        
    }
                                      
                                      
        
    
    
    
    
    
    
    
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcard()
        newCard.term = "Term \(cards.count + 1)"
        newCard.definition = "Definition \(cards.count + 1)"
        cards.append(newCard)
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(item: tableView.numberOfRows(inSection: tableView.numberOfSections - 1) - 1, section: tableView.numberOfSections - 1), at: .bottom, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
            Flashcard clicked
         */
        createAlert_1(indexPath: indexPath)
        
        
    
    }
    
    
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
    
    
    
    
    
    
    
}
